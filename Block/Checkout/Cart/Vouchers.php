<?php

namespace Spaaza\Demo\Block\Checkout\Cart;

use Magento\Framework\View\Element\Template;
use Spaaza\Loyalty\Api\Data\VoucherInterface;

class Vouchers extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Spaaza\Loyalty\Model\VoucherManagement
     */
    private $voucherManagement;

    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    private $customerSession;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    private $priceCurrency;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    public function __construct(
        Template\Context $context,
        \Spaaza\Loyalty\Model\VoucherManagement $voucherManagement,
        \Magento\Customer\Helper\Session\CurrentCustomer $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->voucherManagement = $voucherManagement;
        $this->customerSession = $customerSession;
        $this->priceCurrency = $priceCurrency;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @return VoucherInterface[]
     */
    public function getAvailableVouchers()
    {
        if ($this->customerSession->getCustomerId()) {
            $customer = $this->customerSession->getCustomer();
            if ($customer->getId()) {
                return $this->voucherManagement->getAvailableVouchers($customer, true);
            }
        }
        return [];
    }

    public function getVouchersData()
    {
        $result = [
            'vouchers' => [],
        ];
        foreach ($this->getAvailableVouchers() as $availableVoucher) {
            $voucherData = [
                'key' => $availableVoucher->getVoucherKey(),
                'text' => $availableVoucher->getVoucherText(),
                'amount' => $this->formatPrice($availableVoucher->getVoucherAmount()),
                'type' => $availableVoucher->getVoucherType(),
                'is_claimed' => $this->isVoucherClaimed($availableVoucher),
                'is_locked' => $availableVoucher->getVoucherLocked(),
                'campaign_type' => $availableVoucher->getCampaignType(),
                'actions' => array_values($this->getActions($availableVoucher)),
            ];
            $result['vouchers'][] = $voucherData;
        }
        return $result;
    }

    public function isVoucherClaimed(VoucherInterface $voucher)
    {
        return $this->voucherManagement->isClaimed($voucher);
    }

    public function formatPrice($price)
    {
        return $this->priceCurrency->format($price, false);
    }

    /**
     * @param VoucherInterface $voucher
     * @return array
     */
    public function getActions(VoucherInterface $voucher)
    {
        $result = [];

        if ($this->voucherManagement->canClaim($voucher)) {
            $result['claim'] = [
                'url' => $this->getUrl('spaazademo/voucher/claim', ['key' => $voucher->getVoucherKey()]),
                'title' => 'Claim',
            ];
        } elseif ($this->voucherManagement->isClaimed($voucher)) {
            $result['unclaim'] = [
                'url' => $this->getUrl('spaazademo/voucher/unclaim', ['key' => $voucher->getVoucherKey()]),
                'title' => 'Unclaim',
            ];
        }

        if (!$voucher->getVoucherLocked()) {
            $result['lock'] = [
                'url' => $this->getUrl('spaazademo/voucher/lock', ['key' => $voucher->getVoucherKey()]),
                'title' => 'Lock',
            ];
        }
        return $result;
    }
}
