<?php

namespace Spaaza\Demo\Controller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

abstract class AbstractVoucher extends Action
{
    /**
     * @var \Spaaza\Loyalty\Model\VoucherManagement
     */
    protected $voucherManagement;

    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $customerSession;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @param \Spaaza\Loyalty\Model\VoucherManagement $voucherManagement
     * @param \Magento\Customer\Helper\Session\CurrentCustomer $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param Context $context
     */
    public function __construct(
        \Spaaza\Loyalty\Model\VoucherManagement $voucherManagement,
        \Magento\Customer\Helper\Session\CurrentCustomer $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        Context $context
    ) {
        parent::__construct($context);
        $this->voucherManagement = $voucherManagement;
        $this->customerSession = $customerSession;
        $this->checkoutSession = $checkoutSession;
    }
}
