<?php

namespace Spaaza\Demo\Controller\Voucher;

use Spaaza\Demo\Controller\AbstractVoucher;

class Claim extends AbstractVoucher
{
    public function execute()
    {
        $key = $this->getRequest()->getParam('key');

        $customer = $this->customerSession->getCustomer();
        $quote = $this->checkoutSession->getQuote();

        $voucher = $this->voucherManagement->getVoucher($key, $customer);
        $this->voucherManagement->claimVoucher($voucher, $customer, $quote);

        $this->messageManager->addSuccessMessage('Voucher has been claimed');
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setRefererUrl();
        return $resultRedirect;
    }
}
