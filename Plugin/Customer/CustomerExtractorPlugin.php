<?php

namespace Spaaza\Demo\Plugin\Customer;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\CustomerExtractor;
use Magento\Framework\App\RequestInterface;

class CustomerExtractorPlugin
{
    /**
     * @var \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement
     */
    protected $spaazaDataManagement;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    public function __construct(
        \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement $spaazaDataManagement,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->logger = $logger;
    }

    /**
     * Add Spaaza referral code extension attribute value to Customer
     */
    public function afterExtract(
        CustomerExtractor $subject,
        CustomerInterface $customer,
        string $formCode,
        RequestInterface $request,
        array $attributeValues = []
    ): CustomerInterface {
        // Referral code
        $referralCode = $request->getParam('spaaza_referral_code');
        if (!empty($referralCode)) {
            $extensionAttributes = $this->spaazaDataManagement->applyExtensionAttributes($customer);
            $extensionAttributes->setSignupReferralCode($referralCode);
        }

        // Programme opt in
        $optInValue = $request->getParam('spaaza_opt_in');
        if ($optInValue !== null) {
            $extensionAttributes = $this->spaazaDataManagement->applyExtensionAttributes($customer);
            $extensionAttributes->setProgrammeOptedIn((bool)$optInValue);
        }

        return $customer;
    }
}
