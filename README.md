Magento 2 demo implementation module
====================================

This module shows how you _could_ implement several functionalities of the Spaaza Loyalty module. It's been kept as simple as possible to only show Spaaza specific functionality.

Please see the documentation in the `docs` directory for more information.
