<?php

namespace Spaaza\Demo\ViewModel;

class CustomerAccount implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement
     */
    protected $spaazaDataManagement;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement $spaazaDataManagement
    ) {
        $this->customerSession = $customerSession;
        $this->spaazaDataManagement = $spaazaDataManagement;
    }

    /**
     * Get the 'programme_opted_in' value for the logged in customer (or false if not logged in)
     *
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCurrentOptInValue(): bool
    {
        if ($this->customerSession->isLoggedIn()) {
            $customer = $this->customerSession->getCustomerDataObject();
            $spaazaData = $this->spaazaDataManagement->applyExtensionAttributes($customer);
            return (bool)$spaazaData->getProgrammeOptedIn();
        }
        return false;
    }
}
