Magento 2 demo implementation module
====================================

This module shows how you _could_ implement several functionalities of the Spaaza Loyalty module. It's been kept as simple as possible to only show Spaaza specific functionality.

Requirements
------------

- Magento 2.3+
- Spaaza Magento 2 Module (latest version)

Functionality
-------------

### Claim / unclaim a voucher

The module adds an overview of the available vouchers on the cart page. You can claim / unclaim vouchers from the links in the overview.

**Relevant files:**
```
Controller/Voucher/Claim.php
Controller/Voucher/Unclaim.php
Block/Checkout/Cart/Vouchers.php
view/frontend/layout/checkout_cart_index.xml
view/frontend/templates/checkout/cart/vouchers.phtml
```

### Referral codes

A custom field has been added to the customer registration form where the customer can enter a referral code. It then gets set on the customer's extension attributes and will automatically be sent to Spaaza after the customer registration has been completed.

**Relevant files:**
```
Plugin/Customer/CustomerExtractorPlugin.php
view/frontend/layout/customer_account_create.xml
view/frontend/templates/customer/register_referrer.phtml
```

### Loyalty program opt in

A checkbox has been added to the customer registration form and the customer profile edit form to allow the customer to opt in to the Spaaza loyalty program. It gets processed by the same plugin that processes the referral code.

**Relevant files:**
```
Plugin/Customer/CustomerExtractorPlugin.php
view/frontend/layout/customer_account_create.xml
view/frontend/layout/customer_account_edit.xml
view/frontend/templates/customer/optin-checkbox.phtml
```
